package ru.bokhan.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;

@Component
public final class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String argument() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    @EventListener(condition = "@aboutListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Pavel Bokhan");
        System.out.println("E-MAIL: shyaksh@gmail.com");
    }

}
