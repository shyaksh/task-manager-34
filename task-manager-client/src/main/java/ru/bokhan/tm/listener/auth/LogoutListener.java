package ru.bokhan.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.endpoint.SessionEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.listener.AbstractListener;

@Component
public final class LogoutListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private ICurrentSessionService currentSessionService;

    @NotNull
    @Override
    public String command() {
        return "logout";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Log out.";
    }

    @Override
    @EventListener(condition = "@logoutListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        @Nullable SessionDTO session = currentSessionService.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        sessionEndpoint.closeSession(session);
        currentSessionService.setCurrentSession(null);
        System.out.println("[OK:]");
    }

}
